package com.jsw.xmlexercises;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jsw.xmlexercises.XmlUtils.AemetUtil;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.squareup.picasso.Picasso;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import cz.msebera.android.httpclient.Header;

public class Aemet_Activity extends AppCompatActivity {
    public static final String URL = "http://www.aemet.es/xml/municipios/localidad_29067.xml";
    public static final String TEMPORAL = "tiempo.xml";
    private final int WRITE = 0;
    private final int READ = 1;
    private final int INTERNET = 2;

    Bundle datos;
    AsyncHttpClient cliente;
    private LinearLayout mLayout;

    /**Hoy**/
    private TextView fechaHoy;
    private ImageView hoy_1;
    private ImageView hoy_2;
    private ImageView hoy_3;
    private ImageView hoy_4;
    private TextView tempHoyMin;
    private TextView tempHoyMax;

    /**Mañana**/
    private TextView fechaMañ;
    private ImageView mañ_1;
    private ImageView mañ_2;
    private ImageView mañ_3;
    private ImageView mañ_4;
    private TextView tempMañMin;
    private TextView tempMañMax;

    /**Pasado**/
    private TextView fechaPas;
    private ImageView pas_1;
    private ImageView pas_2;
    private ImageView pas_3;
    private TextView tempPasMin;
    private TextView tempPasMax;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aemet);
        mLayout = (LinearLayout)findViewById(R.id.activity_aemet);

        /**Hoy**/
        fechaHoy = (TextView)findViewById(R.id.tv_fechaHoy);
        hoy_1 = (ImageView)findViewById(R.id.iv_Hoy1);
        hoy_2 = (ImageView)findViewById(R.id.iv_Hoy2);
        hoy_3 = (ImageView)findViewById(R.id.iv_Hoy3);
        hoy_4 = (ImageView)findViewById(R.id.iv_Hoy4);
        tempHoyMax = (TextView)findViewById(R.id.tv_HoyMaxima);
        tempHoyMin = (TextView)findViewById(R.id.tv_hoyMinima);

        /**Mañana**/
        fechaMañ = (TextView)findViewById(R.id.tv_fechaMañana);
        mañ_1 = (ImageView)findViewById(R.id.iv_Mañ1);
        mañ_2 = (ImageView)findViewById(R.id.iv_Mañ2);
        mañ_3 = (ImageView)findViewById(R.id.iv_Mañ3);
        mañ_4 = (ImageView)findViewById(R.id.iv_Mañ4);
        tempMañMax = (TextView)findViewById(R.id.tv_mañMaxima);
        tempMañMin = (TextView)findViewById(R.id.tv_mañMinima);

        /**Pasado**/
        fechaPas = (TextView)findViewById(R.id.tv_fechaPasado);
        pas_1 = (ImageView)findViewById(R.id.iv_Pas1);
        pas_2 = (ImageView)findViewById(R.id.iv_Pas2);
        pas_3 = (ImageView)findViewById(R.id.iv_Pas3);
        tempPasMax = (TextView)findViewById(R.id.tv_pasMaxima);
        tempPasMin = (TextView)findViewById(R.id.tv_pasMinima);


        Calendar fecha = new GregorianCalendar();
        fechaHoy.setText(fecha.get(Calendar.YEAR) + "-" + (fecha.get(Calendar.MONTH)+1) + "-" + fecha.get(Calendar.DATE));
        fechaMañ.setText(fecha.get(Calendar.YEAR) + "-" + (fecha.get(Calendar.MONTH)+1) + "-" + (fecha.get(Calendar.DATE)+1));
        fechaPas.setText(fecha.get(Calendar.YEAR) + "-" + (fecha.get(Calendar.MONTH)+1) + "-" + (fecha.get(Calendar.DATE)+2));

        cliente = new AsyncHttpClient();

        descarga(URL);
    }


    private void mostrar() {
        String dirImgPartida_1 = "http://www.aemet.es/imagenes/png/estado_cielo/";
        String dirImgPartida_2 = "_g.png";

        String hola3 = datos.getString("pas2");
        /**Hoy**/
        Picasso.with(this).load(dirImgPartida_1 + datos.getString("hoy1")+ dirImgPartida_2).into(hoy_1);
        Picasso.with(this).load(dirImgPartida_1 + datos.getString("hoy2") + dirImgPartida_2).into(hoy_2);
        Picasso.with(this).load(dirImgPartida_1 + datos.getString("hoy3") + dirImgPartida_2).into(hoy_3);
        Picasso.with(this).load(dirImgPartida_1 + datos.getString("hoy4") + dirImgPartida_2).into(hoy_4);
        tempHoyMax.setText(datos.getString("maxHoy"));
        tempHoyMin.setText(datos.getString("minHoy"));

        /**Mañana**/
        Picasso.with(this).load(dirImgPartida_1 + datos.getString("mañ1") + dirImgPartida_2).into(mañ_1);
        Picasso.with(this).load(dirImgPartida_1 + datos.getString("mañ2") + dirImgPartida_2).into(mañ_2);
        Picasso.with(this).load(dirImgPartida_1 + datos.getString("mañ3") + dirImgPartida_2).into(mañ_3);
        Picasso.with(this).load(dirImgPartida_1 + datos.getString("mañ4") + dirImgPartida_2).into(mañ_4);
        tempMañMax.setText(datos.getString("maxMañ"));
        tempMañMin.setText(datos.getString("minMañ"));

        /**Pasado**/
        Picasso.with(this).load(dirImgPartida_1 + datos.getString("pas1") + dirImgPartida_2).into(pas_1);
        Picasso.with(this).load(dirImgPartida_1 + datos.getString("pas2") + dirImgPartida_2).into(pas_2);
        Picasso.with(this).load(dirImgPartida_1 + datos.getString("pas3") + dirImgPartida_2).into(pas_3);
        tempPasMax.setText(datos.getString("maxPas"));
        tempPasMin.setText(datos.getString("minPas"));
    }

    private void descarga(String url) {
        final ProgressDialog progreso = new ProgressDialog(this);
        final File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), TEMPORAL);
        RequestHandle requestHandle = cliente.get(url, new FileAsyncHttpResponseHandler(miFichero) {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Fallo: " + throwable.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                Toast.makeText(getApplicationContext(), "Descarga realizada con éxito: " + file.getPath(), Toast.LENGTH_LONG).show();
                try {
                    datos = AemetUtil.datosTiempo(file);
                    mostrar();
                } catch (XmlPullParserException | IOException e) {
                    e.printStackTrace();
                }

                progreso.dismiss();
            }

            @Override
            public void onStart() {
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }
        });
    }
}
