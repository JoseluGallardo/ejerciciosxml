package com.jsw.xmlexercises.XmlUtils;

import android.util.Xml;

import com.jsw.xmlexercises.Model.Estacion;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by joselu on 6/12/16.
 */

public class BiziUtil {

    public ArrayList<Estacion> getEstaciones(File file) throws IOException, XmlPullParserException {
        int eventType;
        ArrayList<Estacion> estaciones = null;
        Estacion actual = null;
        XmlPullParser xpp = Xml.newPullParser();
        xpp.setInput(new FileReader(file));
        eventType = xpp.getEventType();

        boolean dentroEstacion = false;

        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser. START_DOCUMENT :
                    estaciones = new ArrayList<>();
                    break;
                case XmlPullParser. START_TAG :
                    if (xpp.getName().equalsIgnoreCase("estacion")) {
                        dentroEstacion = true;
                        actual = new Estacion();
                    }
                    if (dentroEstacion && xpp.getName().equalsIgnoreCase("uri")) {
                        actual.setUrl(xpp.nextText());
                    }
                    if (dentroEstacion && xpp.getName().equalsIgnoreCase("title")) {
                        actual.setTitulo(xpp.nextText());
                    }
                    if (dentroEstacion && xpp.getName().equalsIgnoreCase("estado")) {
                        actual.setEstado(xpp.nextText());
                    }
                    if (dentroEstacion && xpp.getName().equalsIgnoreCase("bicisDisponibles")) {
                        actual.setDisponibles(xpp.nextText());
                    }
                    break;
                case XmlPullParser. END_TAG :
                    if (xpp.getName().equalsIgnoreCase("estacion")) {
                        dentroEstacion = false;
                        try {
                            estaciones.add(actual);
                        }
                        catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                    break;
            }
            eventType = xpp.next();
        }

        return estaciones;
    }
}
