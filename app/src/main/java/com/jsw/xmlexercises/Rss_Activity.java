package com.jsw.xmlexercises;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.jsw.xmlexercises.Model.Noticia;
import com.jsw.xmlexercises.XmlUtils.RssUtil;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.Inflater;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.HttpClient;

public class Rss_Activity extends ListActivity {

    private BottomNavigationView mBottomBar;
    private ListView mList;
    private ArrayAdapter mAndroid;
    private ArrayAdapter mMundo;
    private ArrayAdapter mCoches;
    private ArrayList<Noticia> mNoticias;
    private final String ANDROID = "http://www.htcmania.com/external.php?forumids=417";
    private final String NOTICIAS = "http://www.elmundotoday.com/feed/";
    private final String COCHES = "http://www.autobild.es/rss";
    private final String TMP = "rss.xml";
    private AsyncHttpClient client;
    private ImageView mLogo;
    private RssUtil util;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss);

        mBottomBar = (BottomNavigationView)findViewById(R.id.bottom_navigation);
        mList = this.getListView();
        mNoticias = new ArrayList();
        client = new AsyncHttpClient();
        util = new RssUtil();
        mLogo = (ImageView)findViewById(R.id.iv_logo);
        mLogo.setImageResource(R.drawable.rss);

        mBottomBar.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_android:
                                mBottomBar.setItemBackgroundResource(R.color.verdeAndroid);
                                getWindow().setNavigationBarColor(getResources().getColor(R.color.verdeAndroid));
                                descarga(ANDROID);
                                mLogo.setImageResource(R.drawable.android);
                                mAndroid = new ArrayAdapter(Rss_Activity.this, android.R.layout. simple_list_item_1 , mNoticias);
                                mList.setAdapter(mAndroid);
                                break;
                            case R.id.action_noticias:
                                mBottomBar.setItemBackgroundResource(R.color.noticias);
                                getWindow().setNavigationBarColor(getResources().getColor(R.color.noticias));
                                descarga(NOTICIAS);
                                mLogo.setImageResource(R.drawable.noticias);
                                mMundo = new ArrayAdapter(Rss_Activity.this, android.R.layout. simple_list_item_1 , mNoticias);
                                mList.setAdapter(mMundo);
                                break;
                            case R.id.action_coches:
                                mBottomBar.setItemBackgroundResource(R.color.coches);
                                getWindow().setNavigationBarColor(getResources().getColor(R.color.coches));
                                descarga(COCHES);
                                mLogo.setImageResource(R.drawable.coches);
                                mCoches = new ArrayAdapter(Rss_Activity.this, android.R.layout. simple_list_item_1 , mNoticias);
                                mList.setAdapter(mCoches);
                                break;
                        }
                        return false;
                    }
                });

        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Uri uri = Uri.parse(mNoticias.get(i).getLink());
                Intent in = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(in);
            }
        });

        mBottomBar.setSelected(true);
    }

    private void descarga(String url) {
        final ProgressDialog progreso = new ProgressDialog(this);
        final File miFichero=new File(Environment.getExternalStorageDirectory().getAbsolutePath(), TMP);
        RequestHandle requestHandle = client.get(url, new FileAsyncHttpResponseHandler(miFichero) {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Fallo: " + throwable.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Descarga realizada con éxito: " + file.getPath(), Toast.LENGTH_LONG).show();
                try {
                    mNoticias = util.getNoticias(file);
                    mAndroid = new ArrayAdapter(Rss_Activity.this, android.R.layout. simple_list_item_1 , mNoticias);
                    mList.setAdapter(mAndroid);
                } catch (XmlPullParserException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }
        });
    }
}
