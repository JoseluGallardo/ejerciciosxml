package com.jsw.xmlexercises.XmlUtils;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.os.Bundle;

import com.jsw.xmlexercises.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by joselu on 6/12/16.
 */

public class XmlUtil {

    public Bundle analizeEmpresa(Context context) throws XmlPullParserException, IOException {

        Bundle potatoe = new Bundle();

        int contadorEmpleados = 0;
        double sueldoMinimo = Double.MAX_VALUE;
        double sueldoMaximo = Double.MIN_VALUE;
        double edadTotal = 0;
        double edadMedia = 0;

        boolean dentroEmpleado = false;

        XmlResourceParser xrp = context.getResources().getXml(R.xml.empresa);
        int eventType = xrp.getEventType();

        while (eventType != XmlPullParser.END_DOCUMENT) {

            switch (eventType) {
                
                case XmlPullParser.START_TAG:
                    if (xrp.getName().equalsIgnoreCase("empleado")) {
                        dentroEmpleado = true;
                        contadorEmpleados += 1;
                    }
                    
                    if (dentroEmpleado && xrp.getName().equalsIgnoreCase("edad")) {
                        double edadTmp = Double.parseDouble(xrp.nextText());
                        edadTotal += edadTmp;
                    }
                    
                    if (dentroEmpleado && xrp.getName().equalsIgnoreCase("salario")) {
                        double sueldoTmp = Double.parseDouble(xrp.nextText());
                        if (sueldoTmp < sueldoMinimo) {
                            sueldoMinimo = sueldoTmp;
                        }
                        if (sueldoTmp > sueldoMaximo) {
                            sueldoMaximo = sueldoTmp;
                        }
                    }
                    break;

                case XmlPullParser.END_TAG:
                    if (xrp.getName().equalsIgnoreCase("empleado")) {
                        dentroEmpleado = false;
                    }
                    break;
            }

            eventType = xrp.next();
        }
        
        if (contadorEmpleados != 0) {
            edadMedia = (edadTotal / contadorEmpleados);
            potatoe.putString("numero", String.valueOf(contadorEmpleados));
            potatoe.putString("media", String.valueOf(edadMedia));
            potatoe.putString("max", String.valueOf(sueldoMaximo));
            potatoe.putString("min", String.valueOf(sueldoMinimo));

        }
        else {
            potatoe.putString("numero", "0");
            potatoe.putString("media", "0");
            potatoe.putString("max", "0");
            potatoe.putString("min", "0");
        }

        return potatoe;
    }
}
