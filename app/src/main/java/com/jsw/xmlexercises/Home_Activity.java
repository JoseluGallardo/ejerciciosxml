package com.jsw.xmlexercises;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Home_Activity extends AppCompatActivity {

    private Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    public void launchXml(View v){
        i = new Intent(this, Xml_Activity.class);
        startActivity(i);
    }

    public void launchTiempo(View v){
        i = new Intent(this, Aemet_Activity.class);
        startActivity(i);
    }

    public void launchBizi(View v){
        i = new Intent(this, Bizi_Activity.class);
        startActivity(i);
    }

    public void launchRss(View v){
        i = new Intent(this, Rss_Activity.class);
        startActivity(i);
    }
}
