package com.jsw.xmlexercises.Model;

/**
 * Created by joselu on 6/12/16.
 */

public class Estacion {
    String titulo;

    public String getTitulo() {
        return titulo;
    }

    public String getEstado() {
        return estado;
    }

    public String getDisponibles() {
        return disponibles;
    }

    public String getUrl() {
        return url;
    }

    String estado;
    String disponibles;
    String url;

    public Estacion(){

    }
    public Estacion(String titulo, String estado, String bicisD, String url) {
        this.titulo = titulo;
        this.estado = estado;
        this.disponibles = bicisD;
        this.url = url;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setDisponibles(String bicis) {
        this.disponibles = bicis;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
