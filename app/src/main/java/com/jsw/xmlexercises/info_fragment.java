package com.jsw.xmlexercises;


import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jsw.xmlexercises.Model.Estacion;
import com.jsw.xmlexercises.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class info_fragment extends DialogFragment {


    private Estacion estacion;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ProductHolder p = new ProductHolder();
        View rootView = inflater.inflate(R.layout.fragment_info, container, false);
        p.estacion = (TextView) rootView.findViewById(R.id.tv_estacion);
        p.estado = (TextView) rootView.findViewById(R.id.tv_estado);
        p.nBicis = (TextView) rootView.findViewById(R.id.tv_disponibles);

        p.estacion.setText(estacion.getTitulo());
        p.estado.setText(estacion.getEstado());
        p.nBicis.setText(estacion.getDisponibles());
        rootView.setTag(p);
        return rootView;
    }

    public void setEstacion (Estacion estacion){
        this.estacion = estacion;
    }


    class ProductHolder{
        TextView estacion, estado, nBicis;
    }

}
