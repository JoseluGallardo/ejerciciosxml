package com.jsw.xmlexercises;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.jsw.xmlexercises.XmlUtils.XmlUtil;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

public class Xml_Activity extends AppCompatActivity {

    private TextView mTvNumero;
    private TextView mTvMedia;
    private  TextView mTvMax;
    private  TextView mTvMin;
    private XmlUtil adapter;

    Bundle datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xml);
        adapter = new XmlUtil();

        try {
            datos = adapter.analizeEmpresa(this);
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }

        mTvNumero = (TextView)findViewById(R.id.tv_num);
        mTvMedia = (TextView)findViewById(R.id.tv_media);
        mTvMax = (TextView)findViewById(R.id.tv_max);
        mTvMin = (TextView)findViewById(R.id.tv_min);
    }

    public void calcular(View v){
        mTvNumero.setText(datos.getString("numero"));
        mTvMedia.setText(datos.getString("media"));
        mTvMax.setText(datos.getString("max"));
        mTvMin.setText(datos.getString("min"));
    }
}
