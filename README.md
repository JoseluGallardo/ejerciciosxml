# README #

## Ejercicios XML ##
### José Luis del Pino Gallardo ###


### Menú principal ###

* Contiene distintos botones que acceden a sus respectivas actividades.

![Screenshot_20161207-105122.png](https://bitbucket.org/repo/zE45dA/images/497709598-Screenshot_20161207-105122.png)


### Ejercicio 1 ###

* Contiene un fichero XML alojado en RES sobre el que se realizan operaciones estadísticas.

![Screenshot_20161207-105126.png](https://bitbucket.org/repo/zE45dA/images/1671096966-Screenshot_20161207-105126.png)

![Screenshot_20161207-105129.png](https://bitbucket.org/repo/zE45dA/images/2698028791-Screenshot_20161207-105129.png)

### Ejercicio 2 ###

* Muestra la previsión del tiempo para hoy, mañana y pasado.

![Screenshot_20161207-105134.png](https://bitbucket.org/repo/zE45dA/images/3148242401-Screenshot_20161207-105134.png)

### Ejercicio 3 ###

* Muestra la lista de estaciones de BIZI en Zaragoza.

*![Screenshot_20161207-105139.png](https://bitbucket.org/repo/zE45dA/images/1320561069-Screenshot_20161207-105139.png)

* Al hacer click sobre una estación podemos ver su estado:

![Screenshot_20161207-105147.png](https://bitbucket.org/repo/zE45dA/images/1214798931-Screenshot_20161207-105147.png)

* Al hacer una pulsación prolongada sobre una estación podemos ver su ubicación en el mapa

![Screenshot_20161207-105155.png](https://bitbucket.org/repo/zE45dA/images/498870574-Screenshot_20161207-105155.png)

### Ejercicio 3 ###

* Muestra un lector de noticias RSS conectado a tres medios distintos.

![Screenshot_20161207-105202.png](https://bitbucket.org/repo/zE45dA/images/3211932673-Screenshot_20161207-105202.png)

* Pulsando en el menú inferior podemos cambiar entre proveedor de contenidos.

![Screenshot_20161207-105205.png](https://bitbucket.org/repo/zE45dA/images/2741798027-Screenshot_20161207-105205.png)

![Screenshot_20161207-105213.png](https://bitbucket.org/repo/zE45dA/images/3918602080-Screenshot_20161207-105213.png)