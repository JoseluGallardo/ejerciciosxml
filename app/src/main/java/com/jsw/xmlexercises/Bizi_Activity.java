package com.jsw.xmlexercises;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.jsw.xmlexercises.Model.Estacion;
import com.jsw.xmlexercises.XmlUtils.BiziUtil;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class Bizi_Activity extends ListActivity {

    public static final String URL = "http://www.zaragoza.es/api/recurso/urbanismo-infraestructuras/estacion-bicicleta.xml";
    public static final String TMP = "bicis.xml";

    private ArrayList<Estacion> listaEstaciones;
    private ArrayList listaAdapter;
    private ArrayAdapter adapter;
    private ListView lstBicis;
    private AsyncHttpClient cliente;
    private BiziUtil util;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bizi);

        util = new BiziUtil();
        listaAdapter = new ArrayList();
        lstBicis = getListView();
        descarga(URL);

        lstBicis.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                info_fragment fragment = new info_fragment ();
                fragment.setEstacion(listaEstaciones.get(i));
                fragment.show(getFragmentManager(), " ");
            }
        });

        lstBicis.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Uri uri = Uri.parse(listaEstaciones.get(i).getUrl());
                Intent in = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(in);
                return true;
            }
        });
    }

    private void descarga(String url) {
        final ProgressDialog progreso = new ProgressDialog(this);
        File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), TMP);
        cliente = new AsyncHttpClient();
        RequestHandle requestHandle = cliente.get(url, new FileAsyncHttpResponseHandler(miFichero) {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Fallo: " + throwable.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Descarga realizada con éxito: " + file.getPath(), Toast.LENGTH_LONG).show();
                try {
                    listaEstaciones = util.getEstaciones(file);
                    for(Estacion est : listaEstaciones)
                        listaAdapter.add(est.getTitulo());
                    adapter = new ArrayAdapter(Bizi_Activity.this, R.layout.support_simple_spinner_dropdown_item, listaAdapter);
                    lstBicis.setAdapter(adapter);
                } catch (XmlPullParserException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }
        });
    }
}
