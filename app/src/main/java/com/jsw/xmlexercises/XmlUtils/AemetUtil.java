package com.jsw.xmlexercises.XmlUtils;

import android.os.Bundle;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by joselu on 6/12/16.
 */

public class AemetUtil {
    public static Bundle datosTiempo(File file) throws IOException, XmlPullParserException {

        Bundle potatoe = new Bundle();
        String maximaMañ = "";

        boolean dentroTemperatura = false;

        int eventType;
        XmlPullParser xrp = Xml.newPullParser();
        xrp.setInput(new FileReader(file));
        eventType = xrp.getEventType();

        int contadorDias = 0;
        while (eventType != XmlPullParser.END_DOCUMENT  && contadorDias < 3) {

            switch (eventType) {
                case XmlPullParser. START_DOCUMENT :
                    break;
                case XmlPullParser.START_TAG:
                    if(xrp.getName().equalsIgnoreCase("estado_cielo")) {
                        String periodo = xrp.getAttributeValue(0);

                        if (periodo.equals("00-06") && contadorDias == 0)
                            potatoe.putString("hoy1", xrp.nextText());

                        if (periodo.equals("06-12") && contadorDias == 0)
                            potatoe.putString("hoy2", xrp.nextText());

                        if (periodo.equals("12-18") && contadorDias == 0)
                            potatoe.putString("hoy3", xrp.nextText());

                        if (periodo.equals("18-24") && contadorDias == 0)
                            potatoe.putString("hoy4", xrp.nextText());

                        if (periodo.equals("00-06") && contadorDias == 1)
                            potatoe.putString("mañ1", xrp.nextText());

                        if (periodo.equals("06-12") && contadorDias == 1)
                            potatoe.putString("mañ2", xrp.nextText());

                        if (periodo.equals("12-18") && contadorDias == 1)
                            potatoe.putString("mañ3", xrp.nextText());

                        if (periodo.equals("18-24") && contadorDias == 1)
                            potatoe.putString("mañ4", xrp.nextText());

                        if (periodo.equals("00-24") && contadorDias == 2)
                            potatoe.putString("pas1", xrp.nextText());

                        if (periodo.equals("00-12") && contadorDias == 2)
                            potatoe.putString("pas2", xrp.nextText());

                        if (periodo.equals("12-24") && contadorDias == 2)
                            potatoe.putString("pas3", xrp.nextText());
                    }
                    if(xrp.getName().equalsIgnoreCase("temperatura")) {
                        dentroTemperatura = true;
                    }
                    if(xrp.getName().equalsIgnoreCase("maxima") && dentroTemperatura) {

                        if (contadorDias == 0)
                            potatoe.putString("maxHoy", xrp.nextText());
                        else if (contadorDias == 1)
                            potatoe.putString("maxMañ", xrp.nextText());
                        else if (contadorDias == 2)
                            potatoe.putString("maxPas", xrp.nextText());

                    }
                    if(xrp.getName().equalsIgnoreCase("minima") && dentroTemperatura) {
                        if (contadorDias == 0)
                            potatoe.putString("minHoy", xrp.nextText());
                        else if (contadorDias == 1)
                            potatoe.putString("minMañ", xrp.nextText());
                        else if (contadorDias == 2)
                            potatoe.putString("minPas", xrp.nextText());
                    }
                    break;
                case XmlPullParser.END_TAG:
                    if(xrp.getName().equalsIgnoreCase("dia")) {
                        contadorDias++;
                    }
                    if(xrp.getName().equalsIgnoreCase("temperatura")) {
                        dentroTemperatura = false;
                    }
                    break;
            }

            eventType = xrp.next();
        }

        return potatoe;
    }
}
