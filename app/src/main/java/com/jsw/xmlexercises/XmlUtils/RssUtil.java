package com.jsw.xmlexercises.XmlUtils;

import android.util.Xml;

import com.jsw.xmlexercises.Model.Noticia;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by joselu on 6/12/16.
 */

public class RssUtil {
    public ArrayList<Noticia> getNoticias(File file) throws XmlPullParserException, IOException {

        int eventType;
        ArrayList<Noticia> noticias = null;
        Noticia actual = null;
        XmlPullParser xpp = Xml.newPullParser();
        xpp.setInput(new FileReader(file));
        eventType = xpp.getEventType();

        boolean dentroItem = false;

        while (eventType != XmlPullParser.END_DOCUMENT) {

            switch (eventType) {
                case XmlPullParser.START_DOCUMENT:
                    noticias = new ArrayList<>();
                    break;
                case XmlPullParser.START_TAG:
                    if (xpp.getName().equalsIgnoreCase("item")) {
                        dentroItem = true;
                        actual = new Noticia();
                    }
                    if (dentroItem && xpp.getName().equalsIgnoreCase("title")) {
                        actual.setTitle(xpp.nextText());
                    }
                    if (dentroItem && xpp.getName().equalsIgnoreCase("link")) {
                        actual.setLink(xpp.nextText());
                    }
                    if (dentroItem && xpp.getName().equalsIgnoreCase("description")) {
                        actual.setDescription(xpp.nextText());
                    }
                    if (dentroItem && xpp.getName().equalsIgnoreCase("pubDate")) {
                        actual.setPubDate(xpp.nextText());
                    }
                    break;
                case XmlPullParser.END_TAG:
                    if (xpp.getName().equalsIgnoreCase("item")) {
                        dentroItem = false;
                        try {
                            noticias.add(actual);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                    break;
            }
            eventType = xpp.next();
        }

        return noticias;
    }
}
